FROM php:7.2-fpm

RUN apt-get update && \
    apt-get install -y \
        zlib1g-dev \
        libpq-dev \
        git \
        unzip

RUN set -xe \
    && docker-php-ext-install \
        bcmath \
        zip \
        pdo_pgsql

RUN curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/bin/ --filename=composer

RUN pecl install xdebug \
    && docker-php-ext-enable \
        xdebug

WORKDIR /var/www
