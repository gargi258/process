<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga;

use PHPUnit\Framework\TestCase;
use Process\Message;
use Process\Saga;
use Process\Saga\Exception\NotFound;
use Process\Test\Saga\Builder;
use Process\Test\Saga\MessageWhichSagaDoesNotListenFor;
use Process\Test\Saga\MessageWhichSagaListeningForAndHandleMethodDoesNotExist;
use Process\Test\Saga\MessageWhichSagaListeningForAndHandleMethodExist;
use stdClass;

class SagaHandlingMessageTest extends TestCase
{
    /** @var Saga */
    private $saga;
    /** @var Message\Stream */
    private $stream;
    /** @var stdClass */
    private $state;

    public function setUp(): void
    {
        $this->stream = new Message\Stream('some-process-id', 'some-name');
        [$this->saga, $this->state] = (new Builder())
            ->withMessageStream($this->stream)
            ->build();
    }

    /**
     * @test
     * @dataProvider messageCount
     */
    public function whenSagaIsListeningForMessageAndHandleMethodExistThenMessageIsHandled(int $messageCount): void
    {
        for ($i = 0; $i < $messageCount; $i++) {
            $this->saga->process(new MessageWhichSagaListeningForAndHandleMethodExist(
                'some-process-id',
                "message-id-$i"
            ));
        }

        self::assertSame($messageCount, $this->state->counter);
    }

    /** @test */
    public function whenSagaIsListeningForMessageAndHandleMethodNonExistThenThrowsException(): void
    {
        $message = new MessageWhichSagaListeningForAndHandleMethodDoesNotExist(
            'some-process-id',
            'message-id'
        );

        $this->expectException(NotFound::class);

        $this->saga->process($message);
    }

    /** @test */
    public function whenSagaIsListeningForTimeMessageAndHandleMethodExistThenHandleMessage(): void
    {
        $firstFirstMessage = new Message\TimeEvent(
            'some-process-id',
            'message-id-1',
            'FirstTimeMessage'
        );
        $secondTimeMessage = new Message\TimeEvent(
            'some-process-id',
            'message-id-2',
            'SecondTimeMessage'
        );

        $this->saga->process($firstFirstMessage);
        $this->saga->process($secondTimeMessage);

        self::assertSame(2, $this->state->counter);
    }

    /** @test */
    public function whenMessageWithProcessIdSameAsSagaOccurThenMessageIsHandled(): void
    {
        $message = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-process-id',
            'message-id'
        );

        $this->saga->process($message);

        self::assertCount(1, $this->stream);
    }

    /** @test */
    public function whenMessageWithProcessIdDifferentAsSagaThenMessageIsDropped(): void
    {
        $message = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-different-process-id',
            'message-id'
        );

        $this->saga->process($message);

        self::assertCount(0, $this->stream);
    }

    /** @test */
    public function doNothingWhenMessageWhichSagaDoesNotListenForOccurred(): void
    {
        $message = new MessageWhichSagaDoesNotListenFor(
            'some-process-id',
            'message-id'
        );

        $this->saga->process($message);

        self::assertSame(0, $this->state->counter);
    }

    /** @test */
    public function whenDuplicatedMessageOccurThenMessageIsDropped(): void
    {
        $message = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-process-id',
            'message-id'
        );
        $this->saga->process($message);

        $this->saga->process($message);

        self::assertSame(1, $this->state->counter);
        self::assertCount(1, $this->stream);
    }

    /** @test */
    public function whenDuplicatedTimeMessageOccurThenMessageIsDropped(): void
    {
        $message = new Message\TimeEvent(
            'some-process-id',
            'message-id',
            'FirstTimeMessage'
        );
        $this->saga->process($message);

        $this->saga->process($message);
        $this->saga->process($message);

        self::assertSame(1, $this->state->counter);
        self::assertCount(1, $this->stream);
    }

    /** @return int[][] */
    public function messageCount(): array
    {
        return [
            [0],
            [1],
            [2],
            [13],
        ];
    }
}
