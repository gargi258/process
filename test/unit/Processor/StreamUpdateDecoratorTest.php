<?php declare(strict_types = 1);

// phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter

namespace Process\Test\Unit\Processor;

use PHPUnit\Framework\TestCase;
use Process\Message;
use Process\Processor;
use Process\Processor\StreamUpdateDecorator;
use Process\Test\Saga\MessageWithDefaultValue;

class StreamUpdateDecoratorTest extends TestCase
{
    /** @test */
    public function whenProcessMessageThenAddProcessedStreamToRepositoryAfterMessageProcessed(): void
    {
        $stream = new Message\Stream('some-id', 'some-name');
        $processor = new class() implements Processor {
            public function process(Message $message): void
            {
            }
        };
        $decorator = new StreamUpdateDecorator($processor, $stream);

        $decorator->process(new MessageWithDefaultValue());

        self::assertCount(1, $stream);
    }

    /** @test */
    public function whenProcessMessageThenProcessMessageByDecoratedProcessor(): void
    {
        $stream = new Message\Stream('some-id', 'some-name');
        $processor = new class() implements Processor {
            /** @var bool */
            private $processed = false;

            public function process(Message $message): void
            {
                $this->processed = true;
            }

            public function processed(): bool
            {
                return $this->processed;
            }
        };
        $decorator = new StreamUpdateDecorator($processor, $stream);

        $decorator->process(new MessageWithDefaultValue());

        self::assertTrue($processor->processed());
    }

    /** @test */
    public function whenExceptionOccurredWhileProcessingMessageThenDoNotAddModifiedStreamToRepository(): void
    {
        $stream = new Message\Stream('some-id', 'some-name');
        $processor = new class() implements Processor {
            public function process(Message $message): void
            {
                throw new \RuntimeException();
            }
        };
        $decorator = new StreamUpdateDecorator($processor, $stream);

        try {
            $decorator->process(new MessageWithDefaultValue());
        } catch (\Throwable $e) {
            self::assertInstanceOf(\RuntimeException::class, $e);
            self::assertCount(0, $stream);

            return;
        }

        throw new \RuntimeException('Assertions are not triggered');
    }
}
