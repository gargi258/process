<?php declare(strict_types = 1);

namespace Process\Test\Integration\Message\Stream\Repository;

use PHPUnit\Framework\TestCase;
use Process\Message\Builder\DummyArray;
use Process\Message\Exception\StreamDoesNotExist;
use Process\Message\Stream;
use Process\Message\Stream\Repository\Pdo;
use Process\Test\Saga\MessageWhichInitializeSaga;
use Process\Test\Saga\MessageWithDefaultValue;
use Ramsey\Uuid\Uuid;

class PdoTest extends TestCase
{
    /** @var \PDO */
    private $pdo;
    /** @var Pdo */
    private $streams;

    protected function setUp(): void
    {
        $this->pdo = new \PDO(
            'pgsql:host=database;dbname=devdb',
            'devdb',
            'devdb'
        );
        $this->streams = new Pdo($this->pdo, new DummyArray());
        $this->pdo->exec('TRUNCATE TABLE messages');
        $this->pdo->exec('TRUNCATE TABLE streams');
    }

    /** @test */
    public function whenGenNewTHenReturnNewStream(): void
    {
        $stream = $this->streams->getNew(
            'stream-name',
            new MessageWithDefaultValue()
        );

        self::assertSame('stream-name', $stream->name());
        self::assertSame('process-id-default', $stream->id());
    }

    /** @test */
    public function whenAddNonExistStreamToRepositoryThenStreamWithNameIsSaved(): void
    {
        $stream = new Stream(
            '99ac1176-b42c-46a1-b292-cc0afee3866c',
            'process-name'
        );

        $this->streams->add($stream);

        /** @var \PDOStatement $stmt */
        $stmt = $this->pdo->query(
            'SELECT * FROM streams WHERE stream_id = \'99ac1176-b42c-46a1-b292-cc0afee3866c\''
        );
        $stmt->execute();
        /** @var mixed[] $result */
        $result = $stmt->fetchAll();

        self::assertCount(1, $result);

        $stream = $result[0];
        self::assertEquals('process-name', $stream['name']);
    }

    /**
     * @test
     * @dataProvider messageCount
     */
    public function whenAddNonExistStreamToRepositoryThenStreamMessagesAreStored(
        int $count
    ): void {
        $this->saveAndReturnStream($count);

        /** @var \PDOStatement $stmt */
        $stmt = $this->pdo->query(
            'SELECT * FROM messages WHERE process_id = \'99ac1176-b42c-46a1-b292-cc0afee3866c\''
        );
        $stmt->execute();
        /** @var mixed[] $result */
        $result = $stmt->fetchAll();

        self::assertCount($count, $result);
    }

    /**
     * @test
     * @dataProvider messageCount
     */
    public function whenAddExistingStreamThenAddUnsavedMessagesInStream(
        int $count
    ): void {
        $stream = $this->saveAndReturnStream($count);
        $stream->recordAction(new MessageWithDefaultValue(
            '99ac1176-b42c-46a1-b292-cc0afee3866c'
        ));

        $this->streams->add($stream);

        /** @var \PDOStatement $stmt */
        $stmt = $this->pdo->query(
            'SELECT * FROM messages WHERE process_id = \'99ac1176-b42c-46a1-b292-cc0afee3866c\''
        );
        $stmt->execute();
        /** @var mixed[] $result */
        $result = $stmt->fetchAll();
        self::assertCount($count + 1, $result);
    }

    /**
     * @test
     * @dataProvider messageCount
     */
    public function whenGetExistingStreamFromRepoThenStreamIsReturned(
        int $count
    ): void {
        $stream = $this->saveAndReturnStream($count);

        $downloadedStream = $this->streams->get(
            'process-name',
            new MessageWhichInitializeSaga(
                '99ac1176-b42c-46a1-b292-cc0afee3866c',
                'some-message-id'
            )
        );

        self::assertEquals($stream, $downloadedStream);
    }

    /** @test */
    public function whenGetNonExistingStreamThenThrowsException(): void
    {
        $this->expectException(StreamDoesNotExist::class);

        $this->streams->get(
            'some-name',
            new MessageWhichInitializeSaga(
                'some-process-id',
                'some-message-id'
            )
        );
    }

    /** @return int[][] */
    public function messageCount(): array
    {
        return [
            [0],
            [2],
            [5],
        ];
    }

    public function saveAndReturnStream(int $messageQuantity): Stream
    {
        $stream = new Stream(
            '99ac1176-b42c-46a1-b292-cc0afee3866c',
            'process-name'
        );

        for ($i = 0; $i < $messageQuantity; $i++) {
            $stream->recordAction(new MessageWithDefaultValue(
                '99ac1176-b42c-46a1-b292-cc0afee3866c',
                Uuid::uuid4()->toString()
            ));
        }

        $this->streams->add($stream);

        return $stream;
    }
}
