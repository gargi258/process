<?php declare(strict_types = 1);

namespace Process;

interface Message
{
    public function id(): string;

    public function processId(): string;

    public function name(): string;

    /** @return mixed[] */
    public function payload(): array;

    public function isEqual(Message $message): bool;
}
