<?php declare(strict_types = 1);

namespace Process\Processor;

use Process\Message;
use Process\Processor;

interface Builder
{
    public function build(Message $message, string $sagaName): Processor;
}
