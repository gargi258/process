<?php declare(strict_types = 1);

namespace Process\Test\Unit\Message;

use PHPUnit\Framework\TestCase;
use Process\Message\Exception\StreamIsClosed;
use Process\Message\Stream;
use Process\Test\Saga\MessageWhichSagaListeningForAndHandleMethodExist;

class StreamTest extends TestCase
{
    /** @test */
    public function whenICallCloseStreamThenStreamIsClosed(): void
    {
        $stream = new Stream('some-id', 'some-name');

        $stream->close();

        self::assertTrue($stream->closed());
    }

    /** @test */
    public function streamIsReducedToSingleValue(): void
    {
        $message1 = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'message-id-1'
        );
        $message2 = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'message-id-2'
        );
        $message3 = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'message-id-3'
        );
        $stream = new Stream(
            'some-id',
            'some-name',
            $message1,
            $message2,
            $message3
        );

        $messageCountCalculatedByReduction = $stream->reduce(
            static function ($acc) {
                return ++$acc;
            },
            0
        );

        self::assertSame(3, $messageCountCalculatedByReduction);
    }

    /**
     * @test
     * @dataProvider messagesQuantity
     */
    public function streamRecordActionWhenMessageOccurred(int $messagesQuantity): void
    {
        $stream = new Stream('some-id', 'some-name');

        for ($i = 0; $i < $messagesQuantity; $i++) {
            $stream->recordAction(
                new MessageWhichSagaListeningForAndHandleMethodExist(
                    'some-id',
                    "message-id-$i"
                )
            );
        }

        self::assertSame($messagesQuantity, $stream->reduce(
            static function ($acc) {
                return ++$acc;
            },
            0
        ));
    }

    /** @test */
    public function streamThrowsExceptionWhenTryToRecordMessageInClosedStream(): void
    {
        $stream = new Stream('some-id', 'some-name');
        $stream->close();

        $this->expectException(StreamIsClosed::class);

        $stream->recordAction(new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'message-id'
        ));
    }

    /** @test */
    public function eachFunctionModifyAllMessagesInStream(): void
    {
        $message1 = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'message-id'
        );
        $message2 = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'message-id'
        );
        $message3 = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'message-id'
        );
        $stream = new Stream(
            'some-id',
            'some-name',
            $message1,
            $message2,
            $message3
        );

        $stream->each(static function (MessageWhichSagaListeningForAndHandleMethodExist $message): void {
            $message->someValue = 'a';
        });

        self::assertSame('a', $message1->someValue);
        self::assertSame('a', $message2->someValue);
        self::assertSame('a', $message3->someValue);
    }

    /** @test */
    public function filterFunctionReturnSmallerStreamByCallbackFiltration(): void
    {
        $message1 = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'message-id-1'
        );
        $message1->someValue = 'a';
        $message2 = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'message-id-2'
        );
        $message2->someValue = 'a';
        $message3 = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            'message-id-3'
        );
        $message3->someValue = 'b';
        $stream = new Stream(
            'some-id',
            'some-name',
            $message1,
            $message2,
            $message3
        );

        $filteredStream = $stream->filter(static function (MessageWhichSagaListeningForAndHandleMethodExist $message) {
            return 'a' === $message->someValue;
        });

        self::assertSame(2, $filteredStream->reduce(
            static function ($acc) {
                return ++$acc;
            },
            0
        ));
    }

    /**
     * @test
     * @dataProvider messagesQuantity
     */
    public function iCanAddSameMessagesToStream(int $messagesQuantity): void
    {
        $stream = new Stream('some-id', 'some-name');

        for ($i = 0; $i < $messagesQuantity; $i++) {
            $stream->recordAction(
                new MessageWhichSagaListeningForAndHandleMethodExist(
                    'some-id',
                    "message-id-$i"
                )
            );
        }

        self::assertSame($messagesQuantity, $stream->reduce(
            static function ($acc) {
                return ++$acc;
            },
            0
        ));
    }

    /**
     * @test
     * @dataProvider messagesQuantity
     */
    public function countOdStreamReturnMessageQuantityInStream(int $messagesQuantity): void
    {
        $stream = new Stream('some-id', 'some-name');

        for ($i = 0; $i < $messagesQuantity; $i++) {
            $stream->recordAction(
                new MessageWhichSagaListeningForAndHandleMethodExist(
                    'some-id',
                    "message-id-$i"
                )
            );
        }

        self::assertSame($messagesQuantity, $stream->count());
    }

    /** @test */
    public function hasReturnTrueWhenMessageIsInStream(): void
    {
        $message = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            "message-id"
        );
        $stream = new Stream('some-id', 'some-name', $message);

        $has = $stream->has($message);

        self::assertTrue($has);
    }

    /** @test */
    public function hasReturnFalseWhenMessageIsNotInStream(): void
    {
        $message = new MessageWhichSagaListeningForAndHandleMethodExist(
            'some-id',
            "message-id"
        );
        $stream = new Stream('some-id', 'some-name');

        $has = $stream->has($message);

        self::assertFalse($has);
    }

    /** @return int[][] */
    public function messagesQuantity(): array
    {
        return [
            [0],
            [1],
            [5],
        ];
    }
}
