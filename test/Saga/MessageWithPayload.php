<?php declare(strict_types = 1);

namespace Process\Test\Saga;

final class MessageWithPayload extends Message
{
    /** @var mixed[] */
    private $payload;

    /** @param mixed[] $payload */
    public function __construct(
        string $processId,
        string $messageId,
        array $payload
    ) {
        parent::__construct($processId, $messageId);
        $this->payload = $payload;
    }

    /** @return mixed[] */
    public function payload(): array
    {
        return $this->payload;
    }
}
