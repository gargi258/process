<?php declare(strict_types = 1);

namespace Process;

use Process\Saga\Exception;

// TODO
/** @codeCoverageIgnore */
final class UnsupportedMessage extends Exception
{
    public static function caughtMessage(object $message): self
    {
        return new self(
            'Unsupported message: ' . get_class($message)
        );
    }
}
