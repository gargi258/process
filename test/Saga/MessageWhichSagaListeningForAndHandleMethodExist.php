<?php declare(strict_types = 1);

namespace Process\Test\Saga;

final class MessageWhichSagaListeningForAndHandleMethodExist extends Message
{
    /** @var string */
    public $someValue = 'a';
}
