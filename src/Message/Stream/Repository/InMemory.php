<?php declare(strict_types = 1);

namespace Process\Message\Stream\Repository;

use Countable;
use Process\Message;
use Process\Message\Exception;
use Process\Message\Stream;

final class InMemory implements Stream\Repository, Countable
{
    /** @var mixed[] */
    private $streams = [];

    public function __construct(Message\Stream ...$streams)
    {
        foreach ($streams as $stream) {
            $this->add($stream);
        }
    }

    public function add(Message\Stream $stream): void
    {
        isset($this->streams[$stream->name()][$stream->id()])
            ? $this->addWhenExist($stream)
            : $this->addWhenDoesNotExist($stream);
    }

    public function get(string $name, Message $message): Message\Stream
    {
        if (!isset($this->streams[$name][$message->processId()])) {
            throw Exception\StreamDoesNotExist::withMessage($message);
        }

        return $this->streams[$name][$message->processId()];
    }

    public function count(): int
    {
        return count($this->streams);
    }

    public function getNew(string $name, Message $message): Message\Stream
    {
        return new Message\Stream($message->processId(), $name);
    }

    private function addWhenExist(Stream $stream): void
    {
        $savedStream = $this->streams[$stream->name()][$stream->id()];

        $stream->each(static function (Message $message) use ($savedStream): void {
            if ($savedStream->has($message)) {
                return;
            }

            $savedStream->recordAction($message);
        });
    }

    private function addWhenDoesNotExist(Stream $stream): void
    {
        $this->streams[$stream->name()][$stream->id()] = $stream;
    }
}
