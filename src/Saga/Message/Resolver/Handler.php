<?php declare(strict_types = 1);

namespace Process\Saga\Message\Resolver;

use Process\Message;
use Process\Saga\Exception\NotFound;

interface Handler
{
    /** @throws NotFound */
    public function getHandler(Message $message, string $sagaName): callable;
}
