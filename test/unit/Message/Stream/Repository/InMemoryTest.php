<?php declare(strict_types = 1);

namespace Process\Test\Unit\Message\Stream\Repository;

use PHPUnit\Framework\TestCase;
use Process\Message\Exception\StreamDoesNotExist;
use Process\Message\Stream;
use Process\Message\Stream\Repository\InMemory;
use Process\Test\Saga\MessageWhichInitializeSaga;
use Process\Test\Saga\MessageWithDefaultValue;
use Ramsey\Uuid\Uuid;

class InMemoryTest extends TestCase
{
    /** @test */
    public function whenGenNewTHenReturnNewStream(): void
    {
        $stream = (new InMemory())->getNew(
            'stream-name',
            new MessageWithDefaultValue()
        );

        self::assertSame('stream-name', $stream->name());
        self::assertSame('process-id-default', $stream->id());
    }

    /** @test */
    public function whenAddStreamToRepositoryThenStreamIsStored(): void
    {
        $streams = new InMemory();
        $stream = new Stream('some-process-id', 'some-name');

        $streams->add($stream);

        self::assertCount(1, $streams);
    }

    /** @test */
    public function whenAddExistingStreamThenAddNotDuplicatedMessageToSavedStream(): void
    {
        $duplicatedMessage = new MessageWithDefaultValue(
            'some-process-id',
            Uuid::uuid4()->toString()
        );
        $streams = new InMemory();
        $savedStream = new Stream('some-process-id', 'some-name');
        $savedStream->recordAction($duplicatedMessage);
        $streams->add($savedStream);
        $stream = new Stream('some-process-id', 'some-name');
        $stream->recordAction($duplicatedMessage);
        $stream->recordAction(new MessageWithDefaultValue(
            'some-process-id',
            Uuid::uuid4()->toString()
        ));

        $streams->add($stream);

        self::assertCount(2, $savedStream);
    }

    /** @test */
    public function whenGetExistingStreamFromRepoThenStreamIsReturned(): void
    {
        $streams = new InMemory();
        $stream = new Stream('some-process-id', 'some-name');
        $streams->add($stream);

        $downloadedStream = $streams->get(
            'some-name',
            new MessageWhichInitializeSaga(
                'some-process-id',
                'some-message-id'
            )
        );

        self::assertEquals($stream, $downloadedStream);
    }

    /** @test */
    public function whenGetNonExistingStreamFromRepoThenThrowsException(): void
    {
        $streams = new InMemory();

        $this->expectException(StreamDoesNotExist::class);

        $streams->get(
            'some-name',
            new MessageWhichInitializeSaga(
                'some-process-id',
                'some-message-id'
            )
        );
    }
}
