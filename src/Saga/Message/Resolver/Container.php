<?php declare(strict_types = 1);

namespace Process\Saga\Message\Resolver;

use Process\Message;
use Process\Saga\Exception\NotFound;
use Process\Saga\Message\Resolver\Compensator as CompensatorResolver;
use Process\Saga\Message\Resolver\Handler as HandlerResolver;
use Psr\Container\ContainerInterface;

final class Container implements HandlerResolver, CompensatorResolver
{
    /** @var ContainerInterface */
    private $container;
    /** @var mixed[][] */
    private $handlers;
    /** @var mixed[][] */
    private $compensators;

    /**
     * @param mixed[][] $handlers
     * @param mixed[][] $compensators
     */
    public function __construct(
        ContainerInterface $container,
        array $handlers,
        array $compensators
    ) {
        $this->handlers = $handlers;
        $this->compensators = $compensators;
        $this->container = $container;
    }

    public function getHandler(Message $message, string $sagaName): callable
    {
        if (! array_key_exists($sagaName, $this->handlers)) {
            throw NotFound::sagaDeclaration($sagaName);
        }

        if (! array_key_exists($message->name(), $this->handlers[$sagaName])) {
            throw NotFound::handlerFor($message);
        }

        $handlerName = $this->handlers[$sagaName][$message->name()];

        return $this->container->get($handlerName);
    }

    public function getCompensator(Message $message, string $sagaName): callable
    {
        if (! array_key_exists($sagaName, $this->compensators)) {
            throw NotFound::sagaDeclaration($sagaName);
        }

        if (! array_key_exists($message->name(), $this->compensators[$sagaName])) {
            throw NotFound::handlerFor($message);
        }

        $compensatorName = $this->compensators[$sagaName][$message->name()];

        return $this->container->get($compensatorName);
    }
}
