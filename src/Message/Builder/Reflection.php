<?php declare(strict_types = 1);

namespace Process\Message\Builder;

use Process\Message;
use Process\Message\Builder;
use ReflectionClass;
use ReflectionMethod;
use ReflectionParameter;

final class Reflection implements Builder
{
    /** @param mixed[] $message */
    public function build(array $message): Message
    {
        $messageReflection = new ReflectionClass($message['class']);
        /** @var ReflectionMethod $constructorMethod */
        $constructorMethod = $messageReflection->getConstructor();
        $parameters = $this->collectParameters($constructorMethod, $message);

        return new $message['class'](...$parameters);
    }

    /**
     * @param mixed[] $message
     * @return mixed[]
     */
    private function collectParameters(
        ReflectionMethod $constructor,
        array $message
    ): array {
        $parameters = [];

        foreach ($constructor->getParameters() as $parameter) {
            $name = $parameter->getName();

            if (isset($message[$name])) {
                $parameters[] = $this->buildParameter(
                    $parameter,
                    $message[$name]
                );

                continue;
            }

            if (isset($message['payload'][$name])) {
                $parameters[] = $this->buildParameter(
                    $parameter,
                    $message['payload'][$name]
                );

                continue;
            }

            if ($parameter->isDefaultValueAvailable()) {
                $parameters[] = $parameter->getDefaultValue();

                continue;
            }
        }

        return $parameters;
    }

    /**
     * @param mixed $data
     * @return mixed
     */
    private function buildParameter(ReflectionParameter $parameter, $data)
    {
        $class = $parameter->getClass();

        if (null !== $class) {
            $name = $class->getName();
            /** @var ReflectionMethod $constructor */
            $constructor = $class->getConstructor();
            $parameters = $this->collectParameters($constructor, $data);

            return new $name(...$parameters);
        }

        return $data;
    }
}
