<?php declare(strict_types = 1);

namespace Process\Saga\Resolver;

use Iterator;
use Process\Message;
use Process\Saga\Resolver;

final class InMemory implements Resolver
{
    /** @var mixed */
    private $sagasMap;

    /** @param mixed[] $sagasMap */
    public function __construct(array $sagasMap = [])
    {
        $this->sagasMap = $sagasMap;
    }

    public function getSagas(Message $message): Iterator
    {
        if (!isset($this->sagasMap[$message->name()])) {
            return;
        }

        foreach ($this->sagasMap[$message->name()] as $saga) {
            yield $saga;
        }
    }
}
