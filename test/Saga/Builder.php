<?php declare(strict_types = 1);

namespace Process\Test\Saga;

use Process\Message;
use Process\Saga;
use stdClass;

final class Builder
{
    /** @var Message\Stream */
    private $stream;

    public function withMessageStream(Message\Stream $stream): self
    {
        $this->stream = $stream;

        return $this;
    }

    /** @return mixed[] */
    public function build(): array
    {
        $state = new stdClass();
        $state->counter = 0;
        $handler = static function () use ($state): void {
            $state->counter++;
        };
        $compensator = static function () use ($state): void {
            $state->counter--;
        };
        $resolver = new Saga\Message\Resolver\InMemory(
            [
                'some-saga' => [
                    MessageWhichInitializeSaga::class => $handler,
                    MessageWhichSagaListeningForAndHandleMethodExist::class => $handler,
                    MessageWhichLetDoneSaga::class => $handler,
                    'FirstTimeMessage' => $handler,
                    'SecondTimeMessage' => $handler,
                    MessageWhichTriggerCompensation::class => $handler,
                ],
            ],
            [
                'some-saga' => [
                    MessageWhichInitializeSaga::class => $compensator,
                    MessageWhichSagaListeningForAndHandleMethodExist::class => $compensator,
                    MessageWhichLetDoneSaga::class => $compensator,
                    'FirstTimeMessage' => $compensator,
                    'SecondTimeMessage' => $compensator,
                    MessageWhichTriggerCompensation::class => $compensator,
                ],
            ]
        );

        $state->executed = false;

        $stream = $this->stream ?? new Message\Stream(
            'some-id',
            'some-saga'
        );

        $saga = new Saga(
            'some-saga',
            $stream,
            new Saga\Message\Resolver\ConfigBased(
                new Saga\Config(
                    MessageWhichInitializeSaga::class,
                    [
                        MessageWhichSagaListeningForAndHandleMethodExist::class,
                        MessageWhichLetDoneSaga::class,
                        MessageWhichSagaListeningForAndHandleMethodDoesNotExist::class,
                        'FirstTimeMessage',
                        'SecondTimeMessage',
                    ],
                    [
                        MessageWhichTriggerCompensation::class,
                    ],
                    'some-saga-close-action',
                    'some-saga-close-strategy'
                ),
                $resolver,
                $resolver
            ),
            static function () use ($state): void {
                $state->executed = true;
            },
            static function (Message $message, Message\Stream $stream): bool {
                if (MessageWhichLetDoneSaga::class === $message->name()) {
                    return true;
                }

                return $stream->reduce(
                    static function (bool $acc, Message $message) {
                        if (MessageWhichLetDoneSaga::class === $message->name()) {
                            $acc = true;
                        }

                        return $acc;
                    },
                    false
                );
            }
        );

        return [
            $saga,
            $state,
        ];
    }
}
