<?php declare(strict_types = 1);

namespace Process\Message\Builder;

use Process\Message;
use Process\Message\Builder;

final class DummyArray implements Builder
{
    /** @param mixed[] $message */
    public function build(array $message): Message
    {
        return new $message['message_fqcn'](
            $message['process_id'],
            $message['message_id'],
            json_decode($message['payload'], true),
            $message['name']
        );
    }
}
