<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga;

use PHPUnit\Framework\TestCase;
use Process\Message;
use Process\Saga;
use Process\Test\Saga\Builder;
use Process\Test\Saga\MessageWhichSagaListeningForAndHandleMethodExist;
use Process\Test\Saga\MessageWhichTriggerCompensation;
use stdClass;

class SagaCompensationTest extends TestCase
{
    /** @var Saga */
    private $saga;
    /** @var Message\Stream */
    private $stream;
    /** @var stdClass */
    private $state;

    public function setUp(): void
    {
        $this->stream = new Message\Stream('some-process-id', 'some-name');
        [$this->saga, $this->state] = (new Builder())
            ->withMessageStream($this->stream)
            ->build();
    }

    /**
     * @test
     * @dataProvider messageCount
     */
    public function whenCompensationMessageOccurThenCompensateAllOccurredMessages(int $messageQuantity): void
    {
        for ($i = 0; $i < $messageQuantity; $i++) {
            $message = new MessageWhichSagaListeningForAndHandleMethodExist(
                'some-process-id',
                "message-id-$i"
            );
            $this->saga->process($message);
        }

        $compensationMessage = new MessageWhichTriggerCompensation(
            'some-process-id',
            'message-id'
        );

        $this->saga->process($compensationMessage);

        self::assertSame(0, $this->state->counter);
    }

    /** @test */
    public function whenCompensationMessageIsHandledThenMessageIsSavedInStreamBeforeCompensationSequence(): void
    {
        $compensationMessage = new MessageWhichTriggerCompensation(
            'some-process-id',
            'message-id'
        );

        $this->saga->process($compensationMessage);

        self::assertTrue($this->stream->has($compensationMessage));
    }

    /** @test */
    public function compensateTimeMessageWhenCompensationMessageOccur(): void
    {
        $firstFirstMessage = new Message\TimeEvent(
            'some-process-id',
            'message-id-1',
            'FirstTimeMessage'
        );
        $secondTimeMessage = new Message\TimeEvent(
            'some-process-id',
            'message-id-2',
            'SecondTimeMessage'
        );
        $compensationMessage = new MessageWhichTriggerCompensation(
            'some-process-id',
            'message-id'
        );
        $this->saga->process($firstFirstMessage);
        $this->saga->process($secondTimeMessage);

        $this->saga->process($compensationMessage);

        self::assertSame(0, $this->state->counter);
    }

    /** @return int[][] */
    public function messageCount(): array
    {
        return [
            [0],
            [1],
            [2],
            [13],
        ];
    }
}
