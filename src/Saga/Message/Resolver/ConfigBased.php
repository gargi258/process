<?php declare(strict_types = 1);

namespace Process\Saga\Message\Resolver;

use Process\Message;
use Process\Saga\Config;
use Process\Saga\Exception\NotFound;
use Process\Saga\Message\Resolver;

final class ConfigBased implements Resolver
{
    /** @var Config */
    private $config;
    /** @var Resolver\Handler */
    private $handlers;
    /** @var Resolver\Compensator */
    private $compensators;

    public function __construct(
        Config $config,
        Resolver\Handler $handlers,
        Resolver\Compensator $compensators
    ) {
        $this->handlers = $handlers;
        $this->compensators = $compensators;
        $this->config = $config;
    }

    public function isInitialMessage(Message $message): bool
    {
        return $message->name() === $this->config->initialMessageName();
    }

    public function isCompensationTriggerMessage(Message $message): bool
    {
        return in_array(
            $message->name(),
            $this->config->compensationMessageList(),
            true
        );
    }

    public function canBeProcessed(Message $message): bool
    {
        return in_array(
            $message->name(),
            $this->config->listeningMessageList(),
            true
        )
            || $this->isInitialMessage($message)
            || $this->isCompensationTriggerMessage($message);
    }

    /** @throws NotFound */
    public function getHandler(Message $message, string $sagaName): callable
    {
        return $this->handlers->getHandler($message, $sagaName);
    }

    /** @throws NotFound */
    public function getCompensator(Message $message, string $sagaName): callable
    {
        return $this->compensators->getCompensator($message, $sagaName);
    }
}
