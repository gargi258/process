<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga\Message\Resolver;

use PHPUnit\Framework\TestCase;
use Process\Saga\Exception\NotFound;
use Process\Saga\Message\Resolver\InMemory;
use Process\Test\Saga\MessageWithDefaultValue;
use Process\Test\Saga\MessageWithPayload;

class InMemoryTest extends TestCase
{
    /** @var callable */
    private $handler;
    /** @var callable */
    private $compensator;
    /** @var InMemory */
    private $resolver;

    protected function setUp(): void
    {
        $this->handler = static function (): void {
        };
        $this->compensator = static function (): void {
        };
        $message = new MessageWithDefaultValue();
        $this->resolver = new InMemory(
            [
                'some-saga' => [
                    $message->name() => $this->handler,
                ],
            ],
            [
                'some-saga' => [
                    $message->name() => $this->compensator,
                ],
            ]
        );
    }

    /** @test */
    public function whenHandlerExistForSagaThenReturnHandler(): void
    {
        $message = new MessageWithDefaultValue();

        $handler = $this->resolver->getHandler($message, 'some-saga');

        self::assertSame($this->handler, $handler);
    }

    /** @test */
    public function whenSagaDoesNotExistAndGetHandlerThenThrowException(): void
    {
        $message = new MessageWithDefaultValue();

        self::expectException(NotFound::class);

        $this->resolver->getHandler($message, 'bad-saga');
    }

    /** @test */
    public function whenHandlerDoesNotExistForSagaThenThrowException(): void
    {
        self::expectException(NotFound::class);

        $this->resolver->getHandler(
            new MessageWithPayload('id', 'id', []),
            'some-saga'
        );
    }

    /** @test */
    public function whenCompensatorExistForSagaThenReturnCompensator(): void
    {
        $message = new MessageWithDefaultValue();

        $compensator = $this->resolver->getCompensator($message, 'some-saga');

        self::assertSame($this->compensator, $compensator);
    }

    /** @test */
    public function whenSagaDoesNotExistAndGetCompensatorThenThrowException(): void
    {
        $message = new MessageWithDefaultValue();

        self::expectException(NotFound::class);

        $this->resolver->getCompensator($message, 'bad-saga');
    }

    /** @test */
    public function whenCompensatorDoesNotExistForSagaThenThrowException(): void
    {
        self::expectException(NotFound::class);

        $this->resolver->getCompensator(
            new MessageWithPayload('id', 'id', []),
            'some-saga'
        );
    }
}
