CREATE TABLE messages (
  id SERIAL PRIMARY KEY,
  process_id VARCHAR(36),
  message_id VARCHAR(36),
  message_fqcn VARCHAR(255),
  name VARCHAR(255),
  payload VARCHAR(500)
);

CREATE TABLE streams (
  stream_id VARCHAR(36) NOT NULL PRIMARY KEY,
  name VARCHAR(255)
);