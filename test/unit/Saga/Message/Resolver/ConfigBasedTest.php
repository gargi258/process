<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga\Message\Resolver;

use PHPUnit\Framework\TestCase;
use Process\Message\TimeEvent;
use Process\Saga\Config;
use Process\Saga\Message\Resolver\ConfigBased;
use Process\Saga\Message\Resolver\InMemory;
use Process\Test\Saga\MessageWhichInitializeSaga;
use Process\Test\Saga\MessageWhichTriggerCompensation;
use Process\Test\Saga\MessageWithDefaultValue;
use Process\Test\Saga\MessageWithPayload;

class ConfigBasedTest extends TestCase
{
    /** @var ConfigBased */
    private $resolver;

    protected function setUp(): void
    {
        $resolver = new InMemory([], []);
        $this->resolver = new ConfigBased(
            new Config(
                MessageWhichInitializeSaga::class,
                [
                    MessageWithDefaultValue::class,
                    'SomeTimeMessage',
                ],
                [
                    MessageWhichTriggerCompensation::class,
                ],
                'non important',
                'non important'
            ),
            $resolver,
            $resolver
        );
    }

    /** @test */
    public function whenMessageIsInitialMessageThenReturnTrue(): void
    {
        $initialMessage = new MessageWhichInitializeSaga(
            'id',
            'id'
        );

        $isInitial = $this->resolver->isInitialMessage($initialMessage);

        self::assertTrue($isInitial);
    }

    /** @test */
    public function whenMessageIsNotInitialMessageThenReturnFalse(): void
    {
        $initialMessage = new MessageWithDefaultValue(
            'id',
            'id'
        );

        $isInitial = $this->resolver->isInitialMessage($initialMessage);

        self::assertFalse($isInitial);
    }

    /** @test */
    public function whenMessageIsCompensationMessageTriggerThenReturnTrue(): void
    {
        $compensationTriggerMessage = new MessageWhichTriggerCompensation(
            'id',
            'id'
        );

        $isCompensation = $this->resolver->isCompensationTriggerMessage(
            $compensationTriggerMessage
        );

        self::assertTrue($isCompensation);
    }

    /** @test */
    public function whenMessageIsNotCompensationMessageTriggerThenReturnFalse(): void
    {
        $isCompensation = $this->resolver->isCompensationTriggerMessage(
            new MessageWithDefaultValue()
        );

        self::assertFalse($isCompensation);
    }

    /** @test */
    public function whenMessageCanBeProcessedThenReturnTrue(): void
    {
        $canBeProcessed = $this->resolver->canBeProcessed(
            new MessageWithDefaultValue()
        );

        self::assertTrue($canBeProcessed);
    }

    /** @test */
    public function whenMessageCanNotBeProcessedThenReturnFalse(): void
    {
        $canBeProcessed = $this->resolver->canBeProcessed(
            new MessageWithPayload('id', 'id', [])
        );

        self::assertFalse($canBeProcessed);
    }

    /** @test */
    public function whenMessageIsInitialButNotListedAsProcessableThenReturnTrue(): void
    {
        $canBeProcessed = $this->resolver->canBeProcessed(
            new MessageWhichInitializeSaga('id', 'id')
        );

        self::assertTrue($canBeProcessed);
    }

    /** @test */
    public function whenMessageIsCompensationButNotListedAsProcessableThenReturnTrue(): void
    {
        $canBeProcessed = $this->resolver->canBeProcessed(
            new MessageWhichTriggerCompensation('id', 'id')
        );

        self::assertTrue($canBeProcessed);
    }

    /** @test */
    public function whenTimeMessageIsProcessableThenReturnTrue(): void
    {
        $canBeProcessed = $this->resolver->canBeProcessed(
            new TimeEvent('id', 'id', 'SomeTimeMessage')
        );

        self::assertTrue($canBeProcessed);
    }
}
