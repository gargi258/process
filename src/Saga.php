<?php declare(strict_types = 1);

namespace Process;

use Process\Saga\Exception;
use Process\Saga\Message\Resolver;

final class Saga implements Processor
{
    /** @var Message\Stream */
    private $stream;
    /** @var Resolver */
    private $resolver;
    /** @var callable */
    private $closeSagaAction;
    /** @var callable */
    private $closeSagaStrategy;
    /** @var string */
    private $name;

    /** @throws Exception\SagaIsClosed */
    public function __construct(
        string $name,
        Message\Stream $stream,
        Resolver $resolver,
        callable $closeSagaAction,
        callable $closeSagaStrategy
    ) {
        if ($stream->closed()) {
            throw Exception\SagaIsClosed::forStream($stream);
        }

        $this->name = $name;
        $this->resolver = $resolver;
        $this->closeSagaAction = $closeSagaAction;
        $this->closeSagaStrategy = $closeSagaStrategy;
        $this->stream = $stream;
    }

    public function process(Message $message): void
    {
        // TODO check saga is open
        if (! $this->canBeHandled($message)) {
            return;
        }

        if ($this->resolver->isCompensationTriggerMessage($message)) {
            $this->compensation($message);

            return;
        }

        $this->stream->recordAction($message);

        ($this->resolver->getHandler($message, $this->name))($message);

        if (!$this->canDone($message, $this->stream)) {
            return;
        }

        ($this->closeSagaAction)($message, $this->stream);
        $this->stream->close();
    }

    private function canBeHandled(Message $message): bool
    {
        if (!$this->resolver->canBeProcessed($message)) {
            return false;
        }

        if ($this->stream->id() !== $message->processId()) {
            return false;
        }

        return !$this->messageAlreadyHandled($message);
    }

    /** @throws Exception */
    private function compensation(Message $triggeringMessage): void
    {
        $this->stream->recordAction($triggeringMessage);

        $this->stream->each(
            function (Message $message) use ($triggeringMessage): void {
                if ($message === $triggeringMessage) {
                    return;
                }

                ($this->resolver->getCompensator($message, $this->name))($message);

                $this->stream->recordAction($message);
            }
        );

        $this->stream->close();
    }

    private function canDone(Message $message, Message\Stream $stream): bool
    {
        return ($this->closeSagaStrategy)($message, $stream);
    }

    private function messageAlreadyHandled(Message $occurredMessage): bool
    {
        $reducer = static function (bool $acc, Message $message) use ($occurredMessage): bool {
            return $message->isEqual($occurredMessage)
                ? true
                : $acc;
        };

        return (bool) $this->stream->reduce($reducer, false);
    }
}
