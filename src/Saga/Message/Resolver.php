<?php declare(strict_types = 1);

namespace Process\Saga\Message;

use Process\Message;
use Process\Saga\Message\Resolver\Compensator;
use Process\Saga\Message\Resolver\Handler;

interface Resolver extends Handler, Compensator
{
    public function canBeProcessed(Message $message): bool;
    public function isInitialMessage(Message $message): bool;
    public function isCompensationTriggerMessage(Message $message): bool;
}
