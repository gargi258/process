<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga;

use PHPUnit\Framework\TestCase;
use Process\Message\Stream;
use Process\Saga;
use Process\Saga\Exception\SagaIsClosed;
use Process\Test\Saga\Builder;
use Process\Test\Saga\MessageWhichLetDoneSaga;
use stdClass;

class SagaClosingTest extends TestCase
{
    /** @var Saga */
    private $saga;
    /** @var Stream */
    private $stream;
    /** @var stdClass */
    private $state;

    public function setUp(): void
    {
        $this->stream = new Stream('some-process-id', 'some-name');
        [$this->saga, $this->state] = (new Builder())
            ->withMessageStream($this->stream)
            ->build();
    }

    /** @test */
    public function whenSagaFitDoneConditionThenSagaCloseActionIsExecuted(): void
    {
        $message = new MessageWhichLetDoneSaga(
            'some-process-id',
            'message-id'
        );

        $this->saga->process($message);

        self::assertTrue($this->state->executed);
    }

    /** @test */
    public function whenSagaFitDoneConditionThenStateIsClosed(): void
    {
        $message = new MessageWhichLetDoneSaga(
            'some-process-id',
            'message-id'
        );

        $this->saga->process($message);

        self::assertTrue($this->stream->closed());
    }

    /** @test */
    public function whenClosedSagaHandleMessageThenThrowsException(): void
    {
        $this->stream->close();

        $this->expectException(SagaIsClosed::class);

        (new Builder())
            ->withMessageStream($this->stream)
            ->build();
    }
}
