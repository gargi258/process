<?php declare(strict_types = 1);

namespace Process\Saga\Message\Resolver;

use Process\Message;
use Process\Saga\Exception\NotFound;

interface Compensator
{
    /** @throws NotFound */
    public function getCompensator(Message $message, string $sagaName): callable;
}
