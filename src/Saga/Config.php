<?php declare(strict_types=1);

namespace Process\Saga;

final class Config
{
    /** @var string */
    private $initialMessageName;
    /** @var string[] */
    private $listeningMessageList;
    /** @var string[] */
    private $compensationMessageList;
    /** @var string */
    private $closeSagaActionName;
    /** @var string */
    private $closeSagaStrategyName;

    public function __construct(
        string $initialMessageName,
        array $listeningMessageList,
        array $compensationMessageList,
        string $closeSagaActionName,
        string $closeSagaStrategyName
    ) {
        $this->initialMessageName = $initialMessageName;
        $this->listeningMessageList = $listeningMessageList;
        $this->compensationMessageList = $compensationMessageList;
        $this->closeSagaActionName = $closeSagaActionName;
        $this->closeSagaStrategyName = $closeSagaStrategyName;
    }

    public function initialMessageName(): string
    {
        return $this->initialMessageName;
    }

    /** @return string[] */
    public function listeningMessageList(): array
    {
        return $this->listeningMessageList;
    }

    /** @return string[] */
    public function compensationMessageList(): array
    {
        return $this->compensationMessageList;
    }

    public function closeSagaActionName(): string
    {
        return $this->closeSagaActionName;
    }

    public function closeSagaStrategyName(): string
    {
        return $this->closeSagaStrategyName;
    }
}
