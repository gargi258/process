<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga\Message\Resolver;

use PHPUnit\Framework\TestCase;
use Process\Saga\Exception\NotFound;
use Process\Saga\Message\Resolver\Container;
use Process\Test\Saga\MessageWithDefaultValue;
use Process\Test\Saga\MessageWithPayload;
use Psr\Container\ContainerInterface;

class ContainerTest extends TestCase
{
    /** @var callable */
    private $handler;
    /** @var callable */
    private $compensator;

    protected function setUp(): void
    {
        $this->handler = static function (): void {
        };
        $this->compensator = static function (): void {
        };
    }

    /** @test */
    public function whenHandlerExistForSagaThenReturnHandler(): void
    {
        $resolver = $this->resolverWith($this->handler);
        $message = new MessageWithDefaultValue();

        $handler = $resolver->getHandler($message, 'some-saga');

        self::assertSame($this->handler, $handler);
    }

    /** @test */
    public function whenSagaDoesNotExistAndGetHandlerThenThrowException(): void
    {
        $resolver = $this->resolverWith($this->handler);
        $message = new MessageWithDefaultValue();

        self::expectException(NotFound::class);

        $resolver->getHandler($message, 'bad-saga');
    }

    /** @test */
    public function whenHandlerDoesNotExistForSagaThenThrowException(): void
    {
        $resolver = $this->resolverWith($this->handler);

        self::expectException(NotFound::class);

        $resolver->getHandler(
            new MessageWithPayload('id', 'id', []),
            'some-saga'
        );
    }

    /** @test */
    public function whenCompensatorExistForSagaThenReturnCompensator(): void
    {
        $resolver = $this->resolverWith($this->compensator);
        $message = new MessageWithDefaultValue();

        $compensator = $resolver->getCompensator($message, 'some-saga');

        self::assertSame($this->compensator, $compensator);
    }

    /** @test */
    public function whenSagaDoesNotExistAndGetCompensatorThenThrowException(): void
    {
        $resolver = $this->resolverWith($this->compensator);
        $message = new MessageWithDefaultValue();

        self::expectException(NotFound::class);

        $resolver->getCompensator($message, 'bad-saga');
    }

    /** @test */
    public function whenCompensatorDoesNotExistForSagaThenThrowException(): void
    {
        $resolver = $this->resolverWith($this->compensator);

        self::expectException(NotFound::class);

        $resolver->getCompensator(
            new MessageWithPayload('id', 'id', []),
            'some-saga'
        );
    }

    private function resolverWith(callable $messageHandler): Container
    {
        $message = new MessageWithDefaultValue();

        return new Container(
            $this->container($messageHandler),
            [
                'some-saga' => [
                    $message->name() => $this->handler,
                ],
            ],
            [
                'some-saga' => [
                    $message->name() => $this->compensator,
                ],
            ]
        );
    }

    // phpcs:disable SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingParameterTypeHint
    // phpcs:disable SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingReturnTypeHint
    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    private function container(callable $toReturn): ContainerInterface
    {
        return new class($toReturn) implements ContainerInterface {
            /** @var callable */
            private $toReturn;

            public function __construct(callable $toReturn)
            {
                $this->toReturn = $toReturn;
            }

            /**
             * @param string $id
             * @return callable
             */
            public function get($id)
            {
                return $this->toReturn;
            }

            /**
             * @param string $id
             * @return bool
             */
            public function has($id)
            {
                throw new \RuntimeException(
                    'Anonymous container \'has\' method not implemented'
                );
            }
        };
    }
    // phpcs:enable SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingParameterTypeHint
    // phpcs:enable SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingReturnTypeHint
    // phpcs:enable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
}
