<?php declare(strict_types = 1);

namespace Process\Test\Unit\Message\Builder;

use PHPUnit\Framework\TestCase;
use Process\Message\Builder\Reflection;
use Process\Test\Saga\FirstSomeClass;
use Process\Test\Saga\MessageWhichLetDoneSaga;
use Process\Test\Saga\MessageWithDefaultValue;
use Process\Test\Saga\MessageWithPayload;
use Process\Test\Saga\MessageWithSomeClassInConstructor;
use Process\Test\Saga\SecondSomeClass;

class ReflectionTest extends TestCase
{
    /** @var Reflection */
    private $builder;

    public function setUp(): void
    {
        $this->builder = new Reflection();
    }

    /** @test */
    public function buildMessageWithTwoScalarValues(): void
    {
        $message = [
            'class' => MessageWhichLetDoneSaga::class,
            'processId' => 'process-id',
            'messageId' => 'message-id',
        ];

        $message = $this->builder->build($message);

        $expectedMessage = new MessageWhichLetDoneSaga(
            'process-id',
            'message-id'
        );
        self::assertEquals($expectedMessage, $message);
    }

    /** @test */
    public function buildMessageWithTwoScalarValuesInPayload(): void
    {
        $message = [
            'class' => MessageWhichLetDoneSaga::class,
            'payload' => [
                'processId' => 'process-id',
                'messageId' => 'message-id',
            ],
        ];

        $message = $this->builder->build($message);

        $expectedMessage = new MessageWhichLetDoneSaga(
            'process-id',
            'message-id'
        );
        self::assertEquals($expectedMessage, $message);
    }

    /** @test */
    public function buildMessageWithConstructorDefaultValues(): void
    {
        $message = [
            'class' => MessageWithDefaultValue::class,
        ];

        $message = $this->builder->build($message);

        $expectedMessage = new MessageWithDefaultValue();
        self::assertEquals($expectedMessage, $message);
    }

    /** @test */
    public function buildMessageWithSomeClassInConstructor(): void
    {
        $message = [
            'class' => MessageWithSomeClassInConstructor::class,
            'obj' => [
                'a' => 'a',
                'b' => 1,
                'secondClass' => [
                    'a' => 'a',
                    'b' => 'b',
                ],
            ],
            'string' => 'string',
            'secondObj' => [
                'a' => 'a',
                'b' => 'b',
            ],
        ];

        $message = $this->builder->build($message);

        $expectedMessage = new MessageWithSomeClassInConstructor(
            new FirstSomeClass(
                'a',
                1,
                new SecondSomeClass('a', 'b')
            ),
            'string',
            new SecondSomeClass('a', 'b')
        );
        self::assertEquals($expectedMessage, $message);
    }

    /** @test */
    public function buildMessageWithPayloadAsArray(): void
    {
        $message = [
            'class' => MessageWithPayload::class,
            'processId' => 'process-id',
            'messageId' => 'message-id',
            'payload' => [
                'item-1' => [],
                'id' => 'some-id',
            ],
        ];

        $message = $this->builder->build($message);

        $expectedMessage = new MessageWithPayload(
            'process-id',
            'message-id',
            [
                'item-1' => [],
                'id' => 'some-id',
            ]
        );
        self::assertEquals($expectedMessage, $message);
    }
}
