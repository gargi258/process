<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga\Builder;

use PHPUnit\Framework\TestCase;
use Process\Message;
use Process\Message\Stream;
use Process\Message\Stream\Repository\InMemory;
use Process\Processor\Saga\Builder\Container;
use Process\Processor\StreamUpdateDecorator;
use Process\Saga\Config;
use Process\Saga\Message\Resolver;
use Process\Test\Saga\Builder;
use Process\Test\Saga\MessageWhichInitializeSaga;
use Process\Test\Saga\MessageWhichLetDoneSaga;
use Process\Test\Saga\MessageWhichSagaListeningForAndHandleMethodDoesNotExist;
use Process\Test\Saga\MessageWhichSagaListeningForAndHandleMethodExist;
use Process\Test\Saga\MessageWhichTriggerCompensation;
use Process\Test\Saga\MessageWithDefaultValue;
use Psr\Container\ContainerInterface;
use stdClass;

class ContainerTest extends TestCase
{
    /** @test */
    public function whenSagaExistForRequestedNameThenReturnIt(): void
    {
        $message = new MessageWithDefaultValue('some-id');
        $config = new Config(
            MessageWhichInitializeSaga::class,
            [
                MessageWhichSagaListeningForAndHandleMethodExist::class,
                MessageWhichLetDoneSaga::class,
                MessageWhichSagaListeningForAndHandleMethodDoesNotExist::class,
                'FirstTimeMessage',
                'SecondTimeMessage',
            ],
            [
                MessageWhichTriggerCompensation::class,
            ],
            'some-saga-close-action',
            'some-saga-close-strategy'
        );
        $stream = new Stream('some-id', 'some-saga');
        $streams = new InMemory($stream);
        $builder = new Container($config, $streams, $this->getContainer());

        $saga = $builder->build($message, 'some-saga');

        // TODO: test it little better
        $expectedSaga = (new Builder())->build()[0];
        $expectedSaga = new StreamUpdateDecorator($expectedSaga, $stream);
        self::assertEquals($expectedSaga, $saga);
    }

    public function getContainer(): ContainerInterface
    {
        // phpcs:disable
        return new class implements ContainerInterface {
            /**
             * @param string $id
             * @return object|callable
             */
            public function get($id)
            {
                if ('some-saga-close-action' === $id) {
                    return static function (): void {
                    };
                }

                if ('some-saga-close-strategy' === $id) {
                    return static function (
                        Message $message,
                        Message\Stream $stream
                    ): bool {
                        return true;
                    };
                }

                $state = new stdClass();
                $state->counter = 0;
                $handler = static function () use ($state): void {
                    $state->counter++;
                };
                $compensator = static function () use ($state): void {
                    $state->counter--;
                };

                $resolver = new Resolver\InMemory(
                    [
                        'some-saga' => [
                            MessageWhichInitializeSaga::class => $handler,
                            MessageWhichSagaListeningForAndHandleMethodExist::class => $handler,
                            MessageWhichLetDoneSaga::class => $handler,
                            'FirstTimeMessage' => $handler,
                            'SecondTimeMessage' => $handler,
                            MessageWhichTriggerCompensation::class => $handler,
                        ],
                    ],
                    [
                        'some-saga' => [
                            MessageWhichInitializeSaga::class => $compensator,
                            MessageWhichSagaListeningForAndHandleMethodExist::class => $compensator,
                            MessageWhichLetDoneSaga::class => $compensator,
                            'FirstTimeMessage' => $compensator,
                            'SecondTimeMessage' => $compensator,
                            MessageWhichTriggerCompensation::class => $compensator,
                        ],
                    ]
                );

                if (Resolver\Handler::class === $id) {
                    return $resolver;
                }

                if (Resolver\Compensator::class === $id) {
                    return $resolver;
                }

                throw new \RuntimeException('Invalid ID in tests');
            }

            /**
             * @param string $id
             * @return bool
             *
             * @throws \RuntimeException
             */
            public function has($id)
            {
                throw new \RuntimeException('Not implemented');
            }
        };
        // phpcs:enable
    }
}
