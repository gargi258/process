<?php declare(strict_types = 1);

namespace Process\Message\Stream\Repository;

use Process\Message;
use Process\Message\Stream;

final class Pdo implements Stream\Repository
{
    private const SAVE_MESSAGE_SQL = '
INSERT INTO messages 
  (process_id, message_id, message_fqcn, name, payload)
VALUES
  (:process_id, :message_id, :message_fqcn, :name, :payload)
';

    private const SAVE_STREAM_SQL = '
INSERT INTO streams 
  (stream_id, name)
VALUES
  (:id, :name)
';

    private const SELECT_STREAM_DATA = '
SELECT stream_id as id, name
FROM streams
WHERE stream_id = :id AND name = :name
';

    private const SELECT_STREAM_MESSAGES_DATA = '
SELECT process_id, message_id, message_fqcn, name, payload
FROM messages
WHERE process_id = :process_id
';

    private const SELECT_STREAM_EXIST = '
SELECT EXISTS(SELECT 1 FROM streams WHERE stream_id=:id and name = :name)
';

    private const SELECT_MESSAGE_EXIST = '
SELECT EXISTS(SELECT 1 FROM messages WHERE message_id=:message_id AND process_id = :process_id)
';

    /** @var \PDO */
    private $pdo;

    /** @var Message\Builder */
    private $builder;

    public function __construct(\PDO $pdo, Message\Builder $builder)
    {
        $pdo->setAttribute(
            \PDO::ATTR_ERRMODE,
            \PDO::ERRMODE_EXCEPTION
        );
        $this->pdo = $pdo;
        $this->builder = $builder;
    }

    public function add(Message\Stream $stream): void
    {
        if (! $this->streamExist($stream)) {
            $this->saveStream($stream);
            $stream->each($this->saveMessageClosure());
        }

        $stream->each($this->saveMessageWhenNotExistClosure());
    }

    public function get(string $name, Message $message): Message\Stream
    {
        $stmt = $this->pdo->prepare(self::SELECT_STREAM_DATA);

        $stmt->execute([
            ':id' => $message->processId(),
            ':name' => $name,
        ]);

        $result = $stmt->fetch();

        if ([] === $result || false === $result) {
            throw Message\Exception\StreamDoesNotExist::withMessage($message);
        }

        return new Stream(
            $result['id'],
            $result['name'],
            ...$this->getMessages($message->processId())
        );
    }

    public function getNew(string $name, Message $message): Message\Stream
    {
        return new Message\Stream($message->processId(), $name);
    }

    private function saveStream(Stream $stream): void
    {
        $stmt = $this->pdo->prepare(self::SAVE_STREAM_SQL);

        $stmt->execute([
            ':id' => $stream->id(),
            ':name' => $stream->name(),
        ]);
    }

    private function saveMessageClosure(): callable
    {
        return function (Message $message): void {
            $stmt = $this->pdo->prepare(self::SAVE_MESSAGE_SQL);

            $stmt->execute([
                ':process_id' => $message->processId(),
                ':message_id' => $message->id(),
                ':message_fqcn' => get_class($message),
                ':name' => $message->name(),
                ':payload' => json_encode($message->payload()),
            ]);
        };
    }

    private function saveMessageWhenNotExistClosure(): callable
    {
        return function (Message $message): void {
            if ($this->messageExist($message)) {
                return;
            }

            $this->saveMessageClosure()($message);
        };
    }

    /** @return Message[] */
    private function getMessages(string $processId): array
    {
        $stmt = $this->pdo->prepare(self::SELECT_STREAM_MESSAGES_DATA);
        $stmt->execute([
            ':process_id' => $processId,
        ]);
        /** @var mixed[] $result */
        $result = $stmt->fetchAll();

        $messages= [];

        foreach ($result as $messageData) {
            $messages[] = $this->builder->build($messageData);
        }

        return $messages;
    }

    private function streamExist(Stream $stream): bool
    {
        $stmt = $this->pdo->prepare(self::SELECT_STREAM_EXIST);

        $stmt->execute([
            ':id' => $stream->id(),
            ':name' => $stream->name(),
        ]);

        /** @var bool $result */
        $result = $stmt->fetchColumn();

        return $result;
    }

    private function messageExist(Message $message): bool
    {
        $stmt = $this->pdo->prepare(self::SELECT_MESSAGE_EXIST);

        $stmt->execute([
            ':message_id' => $message->id(),
            ':process_id' => $message->processId(),
        ]);

        /** @var bool $result */
        $result = $stmt->fetchColumn();

        return $result;
    }
}
