<?php declare(strict_types = 1);

namespace Process\Message;

use Countable;
use Process\Message;

final class Stream implements Countable
{
    /** @var string */
    private $id;
    /** @var Message[] */
    private $messages = [];
    /** @var bool */
    private $closed = false;
    /** @var string */
    private $name;

    public function __construct(string $id, string $name, Message ...$messages)
    {
        $this->id = $id;
        $this->messages = $messages;
        $this->name = $name;
    }

    public function recordAction(Message $message): void
    {
        if ($this->closed) {
            throw new Exception\StreamIsClosed(
                'State is closed and cant record messages'
            );
        }

        $this->messages[] = $message;
    }

    public function has(Message $searchedMessage): bool
    {
        return (bool) $this->reduce(
            static function ($acc, Message $message) use ($searchedMessage) {
                if ($message === $searchedMessage) {
                    $acc = $searchedMessage;
                }

                return $acc;
            }
        );
    }

    public function count(): int
    {
        return (int) $this->reduce(
            static function (int $acc) {
                return ++$acc;
            },
            0
        );
    }

    public function each(callable $callable): void
    {
        array_map($callable, $this->messages);
    }

    public function filter(callable $callable): self
    {
        return new self(
            $this->id,
            $this->name,
            ...array_filter($this->messages, $callable)
        );
    }

    /**
     * @param mixed $defaultValue
     * @return mixed
     */
    public function reduce(callable $callable, $defaultValue = null)
    {
        return null === $defaultValue
            ? array_reduce($this->messages, $callable)
            : array_reduce($this->messages, $callable, $defaultValue);
    }

    public function close(): void
    {
        $this->closed = true;
    }

    public function closed(): bool
    {
        return $this->closed;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }
}
