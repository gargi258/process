<?php declare(strict_types = 1);

namespace Process\Test\Unit\Message;

use PHPUnit\Framework\TestCase;
use Process\Message\TimeEvent;

class TimeEventTest extends TestCase
{
    /** @test */
    public function whenGetPayloadThenPayloadContainFormattedTime(): void
    {
        $event = new TimeEvent('p-id', 'm-id', 'name');

        self::assertSame(
            [
                /**
                 * TODO choose time format for time event
                 * - microtime
                 * - standard datetime?
                 * 'time' => '2019-11-08T17:40:23+00:00',
                 **/
                'processId' => 'p-id',
                'messageId' => 'm-id',
                'name' => 'name',
            ],
            $event->payload()
        );
    }
}
