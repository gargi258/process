<?php declare(strict_types = 1);

// phpcs:disable SlevomatCodingStandard.Classes.SuperfluousExceptionNaming.SuperfluousSuffix

namespace Process\Message;

class Exception extends \Exception
{
}
