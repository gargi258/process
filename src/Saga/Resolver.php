<?php declare(strict_types = 1);

namespace Process\Saga;

use Iterator;
use Process\Message;

interface Resolver
{
    public function getSagas(Message $message): Iterator;
}
