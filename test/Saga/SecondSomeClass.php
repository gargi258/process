<?php declare(strict_types = 1);

namespace Process\Test\Saga;

final class SecondSomeClass
{
    /** @var string */
    private $a;
    /** @var string */
    private $b;

    public function __construct(string $a, string $b)
    {
        $this->a = $a;
        $this->b = $b;
    }
}
