<?php declare(strict_types = 1);

namespace Process\Processor;

use Process\Message;
use Process\Processor;

final class StreamUpdateDecorator implements Processor
{
    /** @var Processor */
    private $processor;
    /** @var Message\Stream */
    private $stream;

    public function __construct(Processor $processor, Message\Stream $stream)
    {
        $this->processor = $processor;
        $this->stream = $stream;
    }

    public function process(Message $message): void
    {
        $this->processor->process($message);

        $this->stream->recordAction($message);
    }
}
