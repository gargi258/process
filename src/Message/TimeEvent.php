<?php declare(strict_types = 1);

namespace Process\Message;

use Process\Message;

final class TimeEvent implements Message
{
    /** @var string */
    private $name;
    /** @var string */
    private $processId;
    /** @var string */
    private $messageId;

    public function __construct(
        string $processId,
        string $messageId,
        string $name
    ) {
        $this->name = $name;
        $this->processId = $processId;
        $this->messageId = $messageId;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function processId(): string
    {
        return $this->processId;
    }

    public function id(): string
    {
        return $this->messageId;
    }

    /** @return string[] */
    public function payload(): array
    {
        return [
            'processId' => $this->processId(),
            'messageId' => $this->id(),
            'name' => $this->name(),
        ];
    }

    public function isEqual(Message $message): bool
    {
        return $this->messageId === $message->id()
            && $this->processId === $message->processId()
            && $this->name() === $message->name();
    }
}
