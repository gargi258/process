<?php declare(strict_types = 1);

namespace Process\Processor\Saga\Builder;

use Process\Message;
use Process\Processor;
use Process\Processor\StreamUpdateDecorator;
use Process\Saga;
use Process\Saga\Message\Resolver;
use Psr\Container\ContainerInterface;

final class Container implements Processor\Builder
{
    /** @var Message\Stream\Repository */
    private $streams;
    /** @var Saga\Config */
    private $config;
    /** @var ContainerInterface */
    private $container;

    public function __construct(
        Saga\Config $config,
        Message\Stream\Repository $streams,
        ContainerInterface $container
    ) {
        $this->streams = $streams;
        $this->config = $config;
        $this->container = $container;
    }

    public function build(Message $message, string $sagaName): Processor
    {
        $stream = $this->streams->get($sagaName, $message);

        $saga = new Saga(
            $sagaName,
            $stream,
            new Resolver\ConfigBased(
                $this->config,
                $this->container->get(Resolver\Handler::class),
                $this->container->get(Resolver\Compensator::class)
            ),
            $this->container->get(
                $this->config->closeSagaActionName()
            ),
            $this->container->get(
                $this->config->closeSagaStrategyName()
            )
        );

        // TODO create decorator resolver
        return new StreamUpdateDecorator($saga, $stream);
    }
}
