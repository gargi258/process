<?php declare(strict_types = 1);

namespace Process\Test\Saga;

final class MessageWithDefaultValue extends Message
{
    public function __construct(
        string $processId = 'process-id-default',
        string $messageId = 'message-id-default'
    ) {
        parent::__construct($processId, $messageId);
    }
}
