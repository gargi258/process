<?php declare(strict_types = 1);

namespace Process\Saga\Exception;

use Process\Message;
use Process\Message\Exception;

class NotFound extends Exception
{
    public static function handlerFor(Message $message): self
    {
        return new self(
            'Not found handler for message: ' . $message->name()
        );
    }

    public static function sagaDeclaration(string $sagaName): self
    {
        return new self(
            'Not found saga declaration with name: ' . $sagaName
        );
    }
}
