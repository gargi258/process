<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga\Resolver;

use PHPUnit\Framework\TestCase;
use Process\Saga\Resolver\InMemory;
use Process\Test\Saga\Builder;
use Process\Test\Saga\MessageWithDefaultValue;

class InMemoryTest extends TestCase
{
    /** @test */
    public function whenSagasAreDeclaredForMessageThenReturnSagas(): void
    {
        [$saga1,] = (new Builder())->build();
        [$saga2,] = (new Builder())->build();
        $resolver = new InMemory([
            MessageWithDefaultValue::class => [
                $saga1,
                $saga2,
            ],
        ]);

        $sagas = $resolver->getSagas(new MessageWithDefaultValue());

        self::assertEquals(
            [
                $saga1,
                $saga2,
            ],
            iterator_to_array($sagas)
        );
    }

    /** @test */
    public function whenSagaIsNotDeclaredForMessageThenReturnEmptyArray(): void
    {
        $resolver = new InMemory();

        $sagas = $resolver->getSagas(new MessageWithDefaultValue());

        self::assertEquals([], iterator_to_array($sagas));
    }
}
