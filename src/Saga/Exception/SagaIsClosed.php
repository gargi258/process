<?php declare(strict_types = 1);

namespace Process\Saga\Exception;

use Process\Message;
use Process\Saga\Exception;

final class SagaIsClosed extends Exception
{
    public static function forStream(Message\Stream $stream): self
    {
        return new self(
            sprintf(
                'Saga can not be initialized with closed stream %s',
                $stream->id()
            )
        );
    }
}
