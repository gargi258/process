<?php declare(strict_types = 1);

namespace Process\Message\Stream;

use Process\Message;
use Process\Message\Exception;

interface Repository
{
    public function add(Message\Stream $stream): void;

    /** @throws Exception\StreamDoesNotExist */
    public function get(string $name, Message $message): Message\Stream;

    // TODO discus below method
    public function getNew(string $name, Message $message): Message\Stream;
}
