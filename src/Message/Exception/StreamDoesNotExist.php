<?php declare(strict_types = 1);

namespace Process\Message\Exception;

use Process\Message;
use Process\Message\Exception;

final class StreamDoesNotExist extends Exception
{
    public static function withMessage(Message $message): self
    {
        return new self(
            sprintf(
                'Stream for processId: %s does not exist, 
                message %s unhandled',
                $message->processId(),
                get_class($message)
            )
        );
    }
}
