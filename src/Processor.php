<?php declare(strict_types = 1);

namespace Process;

interface Processor
{
    public function process(Message $message): void;
}
