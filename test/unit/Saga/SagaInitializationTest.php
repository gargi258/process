<?php declare(strict_types = 1);

namespace Process\Test\Unit\Saga;

use PHPUnit\Framework\TestCase;
use Process\Message;
use Process\Saga;
use Process\Test\Saga\Builder;
use Process\Test\Saga\MessageWhichInitializeSaga;
use stdClass;

class SagaInitializationTest extends TestCase
{
    /** @var Saga */
    private $saga;
    /** @var stdClass */
    private $state;
    /** @var Message\Stream */
    private $stream;

    public function setUp(): void
    {
        $this->stream = new Message\Stream('some-process-id', 'some-name');
        [$this->saga, $this->state] = (new Builder())
            ->withMessageStream($this->stream)
            ->build();
    }

    /** @test */
    public function whenMessageInitializingSagaOccurThenMessageIsHandled(): void
    {
        $message = new MessageWhichInitializeSaga(
            'some-process-id',
            'message-id'
        );

        $this->saga->process($message);

        self::assertSame(1, $this->state->counter);
    }

    /** @test */
    public function whenInitialMessageIsAlreadyHandledThenMessageIsDropped(): void
    {
        $message = new MessageWhichInitializeSaga(
            'some-process-id',
            'message-id'
        );

        $this->saga->process($message);

        self::assertCount(1, $this->stream);
    }
}
