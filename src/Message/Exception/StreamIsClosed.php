<?php declare(strict_types = 1);

namespace Process\Message\Exception;

use Process\Message\Exception;

final class StreamIsClosed extends Exception
{
}
