<?php declare(strict_types = 1);

namespace Process\Message;

use Process\Message;

interface Builder
{
    /** @param mixed[] $message */
    public function build(array $message): Message;
}
