<?php declare(strict_types = 1);

namespace Process\Test\Saga;

final class MessageWhichSagaListeningForAndHandleMethodDoesNotExist extends Message
{
}
