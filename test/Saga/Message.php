<?php declare(strict_types = 1);

namespace Process\Test\Saga;

abstract class Message implements \Process\Message
{
    /** @var string */
    private $processId;
    /** @var string */
    private $messageId;

    public function __construct(string $processId, string $messageId)
    {
        $this->processId = $processId;
        $this->messageId = $messageId;
    }

    public function processId(): string
    {
        return $this->processId;
    }

    public function id(): string
    {
        return $this->messageId;
    }

    /** @return mixed[] */
    public function payload(): array
    {
        return [];
    }

    public function name(): string
    {
        return static::class;
    }

    public function isEqual(\Process\Message $message): bool
    {
        return $this->messageId === $message->id()
            && $this->processId === $message->processId();
    }
}
