<?php declare(strict_types = 1);

namespace Process\Test\Unit\Message\Builder;

use PHPUnit\Framework\TestCase;
use Process\Message\Builder\DummyArray;
use Process\Test\Saga\MessageWithPayload;

class DummyArrayTest extends TestCase
{
    /** @var DummyArray */
    private $builder;

    public function setUp(): void
    {
        $this->builder = new DummyArray();
    }

    /** @test */
    public function buildMessageWithTwoScalarValues(): void
    {
        $message = [
            'message_fqcn' => MessageWithPayload::class,
            'process_id' => 'process-id',
            'message_id' => 'message-id',
            'payload' => '{"a": "payload"}',
            'name' => MessageWithPayload::class,
        ];

        $message = $this->builder->build($message);

        $expectedMessage = new MessageWithPayload(
            'process-id',
            'message-id',
            [ 'a' => 'payload']
        );
        self::assertEquals($expectedMessage, $message);
    }
}
